# Hacking

## Run mTask directly from the git repo

These instructions are written for linux using bash.
With minor adaptations, they can be used for other shells and operating systems as well.

- Install the latest greatest clean

  ```
  mkdir /opt/clean
  curl ftp://ftp.cs.ru.nl/pub/Clean/builds/linux-x64/clean-bundle-complete-linux-x64-latest.tgz | tar -xz --strip-components=1 -C /opt/clean
  echo 'export CLEAN_HOME=/opt/clean' >> .bashrc
  echo 'export PATH=$PATH:$CLEAN_HOME/bin' >> .bashrc
  ```

- Clone the git repo 

  ```
  git clone --recursive https://gitlab.science.ru.nl/mtask/mtask
  ```

- Prepare the dependencies and the dependencies of the dependencies

  ```
  ./linux-compile.sh
  ```

- To regenerate the `bctypes.[ch]` if you changed some types or added instructions

  ```
  cd tools
  cpm project MakeInterpretSymbols create ../mtask-posix.prt
  cpm MakeInterpretSymbols.prj
  ./MakeInterpretSymbols ../mtask-rts
  ```

- Test an example program

  ```
  cd examples
  cpm project test create ../mtask-posix.prt
  cpm test.prj
  ```

## Coding style

For all the C code we adhere to the linux kernel coding style:
https://www.kernel.org/doc/html/v4.10/process/coding-style.html

## Windows

### Setting up iTasks on Windows from a distribution

Download a clean distribution and extract it (e.g. on the Desktop):
https://ftp.cs.ru.nl/Clean/builds/windows-x64/clean-bundle-complete-windows-x64-latest.zip

Download and extract the latest mTask distribution:
https://gitlab.science.ru.nl/mtask/mtask/builds/artifacts/master/raw/mtask-windows-x64.zip?job=windows-x64

Copy the contents of the mTask directory (Examples, Documentation, Config, Libraries) into the clean distribution.

### Compile the test program

- Open the CleanIDE.exe
- File→Open test.icl (from your distribution in the folder Examples\mTask)
- File→New Project using Template...  and select the mTask.prt template (in your distribution in the folder Config\)
- Press Update and Run (CTRL+R) (The first time compiling everything can take a while).
- When the test program starts, make sure to give it the correct permission when the firewall asks for it.

### Run a client

On the PC, open either client-cmd.exe (regular command line client), client-curses.exe (ncurses client using pdcurses) or client-mqtt.exe (MQTT client) from mtask-rts\ in your distribution.

### Setting up mTask from git

There is no definitive guide on how to compile all dependencies on a windows machine since the buildserver crosscompiles everything.
Therefore, the easiest method is to just install mTask as shown above and
replacing pointing the mTask project path in your project to the git
repository.

From the IDE this can be done via: Project→Project Options...
And then in the Project Paths tab, remove {Application}\Libraries\mTask and
append the path to the lib\ folder in your git checkout.

Recompiling the client on windows can be done using MinGW by running `make -f Makefile.pc` in the mtask-rts\ directory.
