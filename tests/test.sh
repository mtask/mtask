#!/bin/bash
set -e
nitrile run mtask-client-desktop mtask-client-tty 2>err.log 1>out.log &
pid="$!"
sleep 1
trap "kill $pid || true" EXIT
timeout 30m tests/TestSuite
sleep 1
