module TestByteCodeEncoding

import StdEnv

import Data.GenDefault

import Data.Func
import Data.Functor
import Data.Either
import Data.Tuple
import Data.List
import Data.Maybe
import Control.Applicative
import Control.GenBimap

import mTask.Interpret.Device
import mTask.Interpret.Specification
import mTask.Interpret.DSL => qualified :: MTask
import mTask.Interpret.ByteCodeEncoding
import mTask.Interpret.Message

import Gast => qualified <., >., <=., >=.
import Gast.CommandLine

import TestCommon
import Text

//derive class Gast MTMessageTo, MTMessageFro, BCPeripheral, Pin
//derive gEq MTMessageTo, MTMessageFro, BCPeripheral, Pin

instance == MTDeviceSpec where (==) x y = x === y
instance == BCShareSpec where (==) x y = x === y
instance == Pin where (==) x y = x === y
instance == APin where (==) x y = x === y
instance == DPin where (==) x y = x === y
instance == MTMessageTo where (==) x y = x === y
instance == MTMessageFro where (==) x y = x === y
instance == (TaskValue a) | gEq{|*|} a where (==) x y = x === y
instance == BCTaskType where (==) x y = x === y
instance == BCPeripheral where (==) x y = x === y

testEncoding` :: (a a -> Property) a -> Property | Gast, fromByteCode{|*|}, toByteCode{|*|}, Eq a
testEncoding` pred a = q (runFBC fromByteCode{|*|} (fromString (toByteCode{|*|} a))) (Right (?Just a), [])
	/\ q (runFBC fromByteCode{|*|} (fromString (toByteCode{|*|} a) ++ ['abc'])) (Right (?Just a), ['abc'])
where
	q (Right (?Just e1), c1) (Right (?Just e2), c2) = pred e1 e2 /\ c1 =.= c2
	q e1 e2 = e1 =.= e2

testEncoding :== testEncoding` (=.=)

m l = [|e\\e<|-l]

Start w = flip (exposeProperties [Concise 99999999] [Tests 10000, Bent]) w $
		[ name "test()"          $ cast () testEncoding
		, name "testInt"         $ (testEncoding For m int16s)
		, name "testLong"        $ cast (Long 0) testEncoding
		, name "testBool"        $ cast True testEncoding
		, name "testChar"        $ (testEncoding For m (map toChar [0..255]))
		, name "testReal"        $ (testEncoding` realprop For m reals)
		, name "testButton"      $ cast NoButton testEncoding
		, name "testUInt8"       $ cast (UInt8 0) testEncoding
		, name "testUInt16"      $ cast (UInt16 0) testEncoding
		, name "testUInt32"      $ cast (UInt32 0) testEncoding
		, name "testInt8"        $ cast (Int8 0) testEncoding
		, name "testInt16"       $ cast (Int16 0) testEncoding
		, name "testInt32"       $ cast (Int32 0) testEncoding
		, name "test2TuplesInt"  $ (testEncoding For tuple <$> m int16s <*> m int16s)
		, name "test2TuplesLng"  $ cast (Long 0, Long 0) testEncoding
		, name "test3TuplesInt"  $ (testEncoding For tuple3 <$> m int16s <*> m int16s <*> m int16s)
		, name "test3TuplesLng"  $ cast (Long 0, Long 0, Long 0) testEncoding
		, name "testAPin"        $ cast A0 testEncoding
		, name "testDPin"        $ cast D0 testEncoding
		, name "testPin"         $ cast (AnalogPin A0) testEncoding
		, name "testGesture"     $ cast GNone testEncoding
		, name "testTaskVal()"   $ cast (Value () False) testEncoding
		, name "testTaskValInt"  $ cast (Value (UInt16 0) False) testEncoding
		, name "testTaskValLng"  $ cast (Value (Long 0) False) testEncoding
		, name "testTaskValTup"  $ cast (Value (UInt16 0, UInt16 0) False) testEncoding
		]
where
	cast :: a -> (a -> Property) -> (a -> Property)
	cast _ = id
derive gDefault MTDeviceSpec
