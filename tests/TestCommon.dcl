definition module TestCommon

from Gast import class Gast, generic gPrint, generic genShow, generic ggen, class PrintOutput, :: PrintState, :: GenState, :: Property

from mTask.Language.Long import :: Long(..)
from mTask.Language.pinIO import :: APin, :: DPin, :: Pin, :: PinMode
from mTask.Language.LCD import :: Button
from mTask.Language.DHT import :: DHTtype, :: DHTInfo
from mTask.Language.Gesture import :: Gesture
from mTask.Language.LEDMatrix import :: LEDMatrixInfo

import mTask.Interpret.String255
import mTask.Interpret.Message
import mTask.Interpret.Instructions
import StdEnv
import Data.UInt

//derive class Gast Either, ?, Button, MTDeviceSpec, MTMessageFro, MTMessageTo, BCInstr, BCShareSpec, BCTaskType, Pin, APin, DPin, BCPeripheral, TaskValue, MTask, PinMode, DHTInfo, LEDMatrixInfo, Gesture, Peripheral
derive class Gast Button, APin, DPin, Gesture, Pin, TaskValue, Either, ?
derive gPrint Long, UInt8, UInt16, UInt32, Int8, Int16, Int32
derive genShow Long, UInt8, UInt16, UInt32, Int8, Int16, Int32
derive ggen Long, UInt8, UInt16, UInt32, Int8, Int16, Int32

int16s :: [Int]
reals :== [0.001, -0.001, 1.23E10, -1.23E10, 1.23E20, -1.23E20:map fromInt ints]
ints  :== [0,1,-1,42,-42,100,-100]
longs :== map Long [20000, -20000:ints]

eps :== 1.19E-07

realeq :: Real Real -> Bool
realprop :: Real Real -> Property
