implementation module TestCommon

import Gast => qualified >., <., <=., >=.
import Gast.Gen

import mTask.Language.GPIO
import mTask.Language.LCD
import mTask.Language.DHT
import mTask.Language.Gesture
import mTask.Language.LEDMatrix

import mTask.Interpret.String255
import mTask.Interpret.Instructions
import StdEnv
import Data.UInt
import Data.Functor

//derive class Gast Either, ?, Button, MTDeviceSpec, MTMessageFro, MTMessageTo, BCInstr, BCShareSpec, BCTaskType, Pin, APin, DPin, BCPeripheral, TaskValue, MTask, PinMode, DHTInfo, LEDMatrixInfo, Gesture, Peripheral, VoidPointer
derive class Gast Button, APin, DPin, Gesture, Pin, TaskValue, Either, ?
derive gPrint Long, UInt8, UInt16, UInt32, Int8, Int16, Int32
derive genShow Long, UInt8, UInt16, UInt32, Int8, Int16, Int32

gmm mn mx = [!mn, mx, one, ~one, zero:[!inc mn..dec (dec mx)]]

ggen{|Long|}   s = [!LONG_MAX, LONG_MIN, zero, one, ~one:[!zero..dec LONG_MAX]]
ggen{|UInt8|}  s = gmm UINT8_MIN UINT8_MAX
ggen{|UInt16|} s = gmm UINT16_MIN UINT16_MAX
ggen{|UInt32|} s = gmm UINT32_MIN UINT32_MAX
ggen{|Int8|}   s = gmm INT8_MIN INT8_MAX
ggen{|Int16|}  s = gmm INT16_MIN INT16_MAX
ggen{|Int32|}  s = gmm INT32_MIN INT32_MAX

int16s :: [Int]
int16s = [i\\(Int16 i)<|-ggen{|*|} genState]

realeq :: Real Real -> Bool
realeq a b
	| a == b = True
	= abs (a-b) <= max (abs a) (abs b) * eps

realprop :: Real Real -> Property
realprop x y = check realeq x y
