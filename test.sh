#!/bin/bash
set -e

CLEAN_HOME=${CLEAN_HOME:-/opt/clean}
MTASKCLIENT=${MTASKCLIENT:-$CLEAN_HOME/mtask-rts/client-tty}
if [ -z $MTASKCLIENT ]
then
	if [ -f $CLEAN_HOME/mtask-rts/client-tty ]
	then
		MTASKCLIENT=$CLEAN_HOME/mtask-rts/client-tty
	elif [ -f $CLEAN_HOME/mtask-rts/client ]
	then
		MTASKCLIENT=$CLEAN_HOME/mtask-rts/client-tty
	else
		echo "cannot find mTask client. CLIENT not set and it's not in CLEAN_HOME/mtask-rts" 2>/dev/null
		exit 1
	fi
fi

cd tests

# Test bytecode encoding
cpm project TestByteCodeEncoding create $CLEAN_HOME/etc/mtask.prt
cpm TestByteCodeEncoding.prj
./TestByteCodeEncoding

# test client
cpm project TestSuite create $CLEAN_HOME/etc/mtask.prt
cpm TestSuite.prj
"$MTASKCLIENT" &
./TestSuite
kill %1
