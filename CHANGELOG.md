# Changelog

# v0.1.2

- Update dependencies

# v0.1.1

- Update dependencies
- Sustrainable 2022 summerschool version

# v0.1

- Initial nitrile version
