# The mTask system (mTasks)

## Introduction

The [mTask system (mTasks)][mtask] is a framework for programming complete Internet of Things (IoT) systems from a single source using task-oriented programming.
The framework is powered by a multi-backend tagless embedded DSL embedded in the pure functional programming language [Clean][clean].

With the bytecode generation backend and the iTask integration, mTask tasks can be embedded in [iTask][itask] programs and shared data sources can be synchronized.
The client runtime system runs on embedded devices such as AVR/ESPxxx/Nucleo boards or on regular devices running linux, windows or mac.

Traditionally IoT architectures are tiered architectures where different languages, protocols and paradigms are used in different layers.
The mTask system generates code for all tiers from a single source specification.

The mTask system is scarcely documented.
Most information can be found in the publications below and from the example programs.

## Layout of the repositories

The mTask group contains four repositories each housing a part of the mTask system:

- [mTask](https://gitlab.com/mtask/mtask), i.e. this repository, contains documentation, tests, and packaging scripts.
- [client](https://gitlab.com/mtask/client) contains the C/C++ source code for the mTask client.
- [server](https://gitlab.com/mtask/client) contains the Clean source code for the mTask server.
- [examples](https://gitlab.com/mtask/examples) contains some example applications.
- [template](https://gitlab.com/mtask/template) contains a ready-to-use template for an mTask application.

## Usage

### Stable™ release

The mTask system is published using [`nitrile`][nitrile] to https://clean-lang.org
Instructions on how to install `nitrile` are available on https://clean-lang.org

To install the desktop client globally (`mtask-client-VARIANT`).

```
nitrile global install mtask-client-desktop
```

To run the client, use:

```
nitrile run mtask-client-desktop mtask-client-VARIANT
```

On windows you have `cmd`, `curses` and `mqtt` variants.
On linux you have `tty`, `curses`, `xcurses`, `mqtt` variants.

### Older releases

Older stable releases were uploaded here:

ftp://ftp.cs.ru.nl/pub/Clean/mTask/

These include a Clean distribution so that they are usable out of the box.

- `2021-June` contains a stable version made in June 2021 for the thesis of Sjoerd Crooijmans (see [Theses](#theses))
- `2020-June` contains a stable version made in June 2020.
- `IOT2020` contains the snapshot used for the IoT 2020 submission.
- `CEFP19` contains the snapshot used for the Central European Functional Programming Summerschool 2019.

### Run mTask directly from the git repo

See https://clean-and-itasks.gitlab.io/nitrile/cli/global-options/#-local-dependency-dep-path on how to include local dependencies when building with `nitrile`.

## Project ideas

- [ ] Exceptions
- [ ] Rewrite only when there are events
    - [x] Based on timing
    - [ ] Based on SDS updates
- [ ] Fixed length updateable data types (e.g. arrays) (in progress, stalled)
- [ ] SDS Lenses
- [ ] Multiple task instances for the same task
- [ ] Buttons in step
- [ ] Device to device communication
- [ ] Preprogrammed mTask tasks (in progress)
- [ ] Intermittent computing capabilities (in progress)
- [ ] Benchmark expressivity (van Aalst workflows?)
- [x] An iTask editor for mTask tasks (in progress)
- [ ] Peripherals
    - [x] Generalized interface for peripherals (in progress)
    - [ ] Screen support
         - [ ] Editors
         - [ ] Layouting (in progress)
    - [ ] PWM support
	- [ ] BTLE support
    - [ ] ...
- [x] Formal semantics (in progress)
- [ ] ...

## Maintainer

Mart Lubbers (mart@cs.ru.nl)

## License

`mtask` is licensed under the BSD 2-Clause "Simplified" License (see [LICENSE](LICENSE)).

## Contributors

- Pieter Koopman
- Mart Lubbers
- Matheus Amazonas Cabral de Andrade
- Erin van der Veen
- Michel de Boer
- Sjoerd Crooijmans
- Elina Antonova

## Publications

### Peer reviewed

- M. Lubbers, P. Koopman, A. Ramsingh, J. Singer, and P. Trinder, ‘Could Tierless Languages Reduce IoT Development Grief?’, ACM Trans. Internet Things, vol. 4, no. 1, Feb. 2023, doi: 10.1145/3572901.
- M. Lubbers, P. Koopman, and R. Plasmeijer, ‘Interpreting Task Oriented Programs on Tiny Computers’, in Proceedings of the 31st Symposium on Implementation and Application of Functional Languages, J. Stutterheim and W. N. Chin, Eds., in IFL ’19. New York, NY, USA: ACM, 2021, p. 12. doi: 10.1145/3412932.3412936.
- M. Lubbers, P. Koopman, A. Ramsingh, J. Singer, and P. Trinder, ‘Tiered versus Tierless IoT Stacks: Comparing Smart Campus Software Architectures’, in Proceedings of the 10th International Conference on the Internet of Things, in IoT ’20. Malmö: ACM, 2020. doi: 10.1145/3410992.3411002.
- M. Lubbers, P. Koopman, and R. Plasmeijer, ‘Multitasking on Microcontrollers using Task Oriented Programming’, in 2019 42nd International Convention on Information and Communication Technology, Electronics and Microelectronics (MIPRO), Opatija, Croatia, May 2019, pp. 1587–1592. doi: 10.23919/MIPRO.2019.8756711.
- P. Koopman and R. Plasmeijer, ‘Type-Safe Functions and Tasks in a Shallow Embedded DSL for Microprocessors’, in Central European Functional Programming School: 6th Summer School, CEFP 2015, Budapest, Hungary, July 6–10, 2015, Revised Selected Papers, V. Zsók, Z. Porkoláb, and Z. Horváth, Eds., Cham: Springer International Publishing, 2019, pp. 283–340. doi: 10.1007/978-3-030-28346-9_8.
- M. Lubbers, P. Koopman, and R. Plasmeijer, ‘Task Oriented Programming and the Internet of Things’, in Proceedings of the 30th Symposium on the Implementation and Application of Functional Programming Languages, Lowell, MA: ACM, 2018, p. 12. doi: 10.1145/3310232.3310239.
- P. Koopman, M. Lubbers, and R. Plasmeijer, ‘A Task-Based DSL for Microcomputers’, in Proceedings of the Real World Domain Specific Languages Workshop 2018 on   - RWDSL2018, Vienna, Austria: ACM Press, 2018, pp. 1–11. doi: 10.1145/3183895.3183902.
- R. Plasmeijer and P. Koopman, ‘A Shallow Embedded Type Safe Extendable DSL for the Arduino’, in Trends in Functional Programming, in Lecture Notes in Computer Science, vol. 9547. Cham: Springer International Publishing, 2016. doi: 10.1007/978-3-319-39110-6.

### Theses

- E. Antonova, ‘mTask Semantics and its Comparison to TopHat’, Bachelor’s Thesis, Radboud University, Nijmegen, 2022.
- S. Crooijmans, ‘Reducing the Power Consumption of IoT Devices in Task-Oriented Programming’, Master’s Thesis, Radboud University, Nijmegen, 2021.
- M. de Boer, ‘Secure Communication Channels for the mTask System.’, Bachelor’s Thesis, Radboud University, Nijmegen, 2020.
- E. van der Veen, ‘Mutable Collection Types in Shallow Embedded DSLs’, Master’s Thesis, Radboud University, Nijmegen, 2020.
- M. Lubbers, ‘Task Oriented Programming and the Internet of Things’, Master’s Thesis, Radboud University, Nijmegen, 2017.
- M. Amazonas Cabral de Andrade, ‘Developing Real Life, Task Oriented Applications for the Internet of Things’, Master’s Thesis, Radboud University, Nijmegen, 2018.

### Under review or in press

- M. Lubbers, P. Koopman, and R. Plasmeijer, ‘Writing Internet of Things Applications with Task Oriented Programming’, in Composability, Comprehensibility and Correctness of Working Software, 8th Summer School, Budapest, Hungary, June 17–21, 2019, Revised Selected Papers, in Lecture Notes in Computer Science, no. 11950. Cham: Springer, 2023, p. 51.
- P. Koopman, M. Lubbers, and R. Plasmeijer, ‘Simulation of a Task-Based Embedded Domain Specific Language for the Internet of Things’, in Composability, Comprehensibility and Correctness of Working Software, 7th Winter School, Kosice, Slovakia, January 22–26, 2018, Revised Selected Papers, in Lecture Notes in Computer Science, no. 11916. Cham: Springer, 2023, p. 51.
- M. Lubbers and P. Koopman, ‘Green Computing for the Internet of Things’, in SusTrainable Summer School 2022, Rijeka, Croatia, July 4–5, 2022, Revised Selected Papers, Cham: Springer International Publishing, 2022, p. 1.

### Datasets

- M. Lubbers, P. Koopman, and R. Plasmeijer, ‘Code for the lecture notes: “Writing Internet of Things Applications with Task Oriented Programming”’. Springer, Feb. 2023. doi: 10.5281/zenodo.7643284.
- M. Lubbers and P. Koopman, ‘Code for the lecture notes: “Green Computing for the Internet of Things”’. Springer, Feb. 2023. doi: 10.5281/zenodo.7643316.
- S. Crooijmans, M. Lubbers, and P. Koopman, ‘Code for the paper “Reducing the Power Consumption of IoT with Task-Oriented Programming”: TFP 2022’. Zenodo, Feb. 2023. doi: 10.5281/zenodo.7634538.
- M. Lubbers, P. Koopman, A. Ramsingh, J. Singer, and P. Trinder, ‘Source code, line counts and memory statistics for CRS, CWS, CRTS and CWTS’. Zenodo, Jun. 2021. doi: 10.5281/zenodo.5040754.
- M. Lubbers, P. Koopman, A. Ramsingh, J. Singer, and P. Trinder, ‘Source code of the PRSS and CWSS applications’. DANS, 2021. doi: 10.17026/dans-zvf-4p9m.
- M. Lubbers, P. Koopman, and R. Plasmeijer, ‘Source code for the interpreted mTask language’. DANS, 2021. doi: 10.17026/dans-zrn-2wv3.
- M. Lubbers, P. Koopman, and R. Plasmeijer, ‘Source code for the mTask language’. DANS, 2020. doi: 10.17026/dans-xx4-8zs9.
- M. Lubbers, P. Koopman, and R. Plasmeijer, ‘Source code for a simplified mTask language integrated with the iTask system’. DANS, 2020. doi: 10.17026/dans-xv6-fvxd.

[clean]: clean.cs.ru.nl
[itask]: clean.cs.ru.nl/ITasks
[mtask]: clean.cs.ru.nl/MTasks
[nitrile]: gitlab.com/clean-and-itasks/nitrile
