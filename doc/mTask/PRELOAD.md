# Preloading tasks

Tasks can be preloaded onto a microcontroller to save bandwidth. Each task is
identified by a hash and when a server wants to start a task on a device, the
device checks whether the task was already preloaded.
