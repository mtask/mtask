\documentclass[a4paper]{article}

\usepackage{geometry}
\usepackage[hidelinks]{hyperref}

\usepackage{glossaries}

\newacronym{ARDSL}{ARDSL}{Arduino \acrlong{DSL}}
\newacronym{DSL}{DSL}{domain specific language}
\newacronym{EDSL}{EDSL}{embedded \acrlong{DSL}}
\newacronym{IOT}{IoT}{Internet of Things}
\newacronym{MCU}{MCU}{microcontroller unit}
\newacronym{SDS}{SDS}{shared data source}
\newacronym{TOP}{TOP}{task-oriented programming}
\newacronym{RTOS}{RTOS}{real-time \acrlong{OS}}
\newacronym{OS}{OS}{operating system}
\newacronym{ML}{ML}{machine learning}
\newglossaryentry{Arduino}{name=Arduino, description={is an ecosystem and a \glsentrytext{C++} dialect for many different types of \acrlongpl{MCU}}}
\newglossaryentry{C}{name=C, description={is an imperative low level system programming language}}
\newglossaryentry{C++}{name=C++, description={is an imperative and object oriented low level programming language compatible with \glsentrytext{C} but supporting much more such as classes.}}
\newglossaryentry{iTask}{name=iTask, description={is a \acrlong{TOP} implementation hosted in the purely lazy functional programming language \glsentrytext{Clean}}}
\newglossaryentry{mTask}{name=mTask, description={is an \acrlong{EDSL} for running tasks on \acrlongpl{MCU}}}
\newglossaryentry{Python}{name=Python, description={is an interpreted high-level general-purpose programing language.}}
\newglossaryentry{MicroPython}{name=MicroPython, description={is a software implementation of a programming language largely compatible with \glsentrytext{Python}, written in \gls{C}, that is optimised to run on a \acrlong{MCU}}}
\newglossaryentry{FreeRTOS}{name=FreeRTOS, description={is a \acrlong{RTOS} kernel for embedded devices.}}
\newglossaryentry{TinyML}{name=TinyML, description={is both a concept and an organisation promoting \acrlong{ML} on edge devices.}}

\title{\glsentrytext{mTask} history}
\author{Mart Lubbers}

\begin{document}
\maketitle

\section{Generating \glsentrytext{C}/\glsentrytext{C++} code}
A first throw at a class-based shallowly \gls{EDSL} for \glspl{MCU} was made by Pieter Koopman and Rinus Plasmijer in 2016~\cite{plasmeijer_shallow_2016}.
The language was called \gls{ARDSL} and offered a type safe interface to \gls{Arduino} \gls{C++} dialect.
A \gls{C++} code generation backend was available together with an \gls{iTask} simulation backend.
There was no support for tasks or even functions.
Some time later in the 2015 CEFP summer school, an extended version was created that allowed the creation of imperative tasks, \glspl{SDS} and the usage of functions~\cite{koopman_type-safe_2019}.
The name then changed from \gls{ARDSL} to \gls{mTask}.

\section{Integration with \glsentrytext{iTask}}
Mart Lubbers extended this in his Master's Thesis by adding integration with \gls{iTask} and a bytecode compiler to the language~\cite{lubbers_task_2017}.
\Gls{SDS} in \gls{mTask} could be accessed on the \gls{iTask} server.
In this way, entire \gls{IOT} systems could be programmed from a single source.
However, this version used a simplified version of \gls{mTask} without functions.
This was later improved upon by creating a simplified interface where \glspl{SDS} from \gls{iTask} could be used in \gls{mTask} and the other way around~\cite{lubbers_task_2018}.
It was shown by Matheus Amazonas Cabral de Andrade that it was possible to build real-life \gls{IOT} systems with this integration~\cite{amazonas_cabral_de_andrade_developing_2018}.
Moreover, a course on the \gls{mTask} simulator was provided at the 2018 CEFP/3COWS winter school in Ko\v{s}ice, Slovakia~\cite{koopman_simulation_2018}.

\section{Transition to \glsentrytext{TOP}}
The \gls{mTask} language as it is now was introduced in 2018~\cite{koopman_task-based_2018}.
This paper updated the language to support functions, tasks and \glspl{SDS} but still compiled to \gls{C++} \gls{Arduino} code.
Later the bytecode compiler and \gls{iTask} integration was added to the language~\cite{lubbers_interpreting_2019}.
Moreover, it was shown that it is very intuitive to write \gls{MCU} applications in a \gls{TOP} language~\cite{lubbers_multitasking_2019}.
One reason for this is that a lot of design patterns that are difficult using standard means are for free in \gls{TOP} (e.g.\ multithreading).
In 2019, the summer school in Budapest, Hungary hosted a course on developing \gls{IOT} applications with \gls{mTask} as well~\cite{lubbers_writing_2019}.
Several students worked on extending \gls{mTask} with many useful features:
Erin van der Veen did preliminary work on a green computer analysis, built a simulator and explored the possibilities for adding bounded datatypes~\cite{veen_van_der_mutable_2020}; Michel de Boer investigated the possibilities for secure communication channels~\cite{boer_de_secure_2020}; and Sjoerd Crooijmans added abstractions for low-power operation to \gls{mTask} such as hardware interrupts and power efficient scheduling~\cite{crooijmans_reducing_2021}.
Moreover, plans for student projects and improvements include exploring integrating \gls{TinyML} into \gls{mTask}; defining a formal semantics for \gls{mTask} and comparing it to TopHat; and adding intermittent computing support to \gls{mTask}.

\section{\glsentrytext{mTask} in practise}
Funded by the Radboud-Glasgow Collaboration Fund, collaborative work was executed with Phil Trinder, Jeremy Singer and Adrian Ravi Kishore Ramsingh.
An existing smart campus application was developed using \gls{mTask} and quantitively and qualitatively compared to the original application that was developed using a traditional \gls{IOT} stack~\cite{lubbers_tiered_2020}.
The collaboration is still ongoing and a journal article is under review comparing four approaches for the edge layer: \gls{Python}, \gls{MicroPython}, \gls{iTask} and \gls{mTask}.
Furthermore, power efficiency behaviour of traditional versus \gls{TOP} \gls{IOT} stacks is being compared as well adding a \gls{FreeRTOS} implementation to the mix as well

\bibliographystyle{plain}
\bibliography{refs}

\end{document}
