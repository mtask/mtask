#!/bin/bash
set -e

suf=tar.gz
arch=x64
os=linux
template=posix
package=-l

if [ $# -eq 1 ]
then
	if [ $1 = "-w" ]
	then
		suf=zip
		arch=x64
		os=windows
		package=-w
		template=windows
	elif [ $1 = "-l" ]
	then
		suf=tar.gz
		arch=x64
		os=linux
		package=-l
		template=posix
	else
		echo "Usage: $0 [-l|-w]"
		exit 1
	fi
fi

TARGET=${TARGET:-mtask}

rm -Irf "$TARGET"
mkdir -p "$TARGET"/etc
for pkg in client server examples
do
	(
		cd $pkg
		./package.sh $package
		mv mtask-$pkg-$os-$arch.$suf ..
	)
	if [ $suf = tar.gz ]
	then
		tar -xzf mtask-$pkg-$os-$arch.$suf --strip-components=1 -C "$TARGET"
	else
		unzip mtask-$pkg-$os-$arch.$suf
		mv mtask-$pkg/* "$TARGET"
		rmdir mtask-$pkg
	fi
done

cp -R doc "$TARGET"
cp -R etc/mtask-$template.prt "$TARGET"/etc/mtask.prt

if [ $os = windows ]
then
	(
		cd "$TARGET"
		mv examples Examples
		mv lib Libraries
		mv etc Config
		mv doc Documentation
	)
fi

# Create archive
if [ $os = linux ]
then
	tar -czf mtask-$os-$arch.$suf "$TARGET"
else
	zip -r mtask-$os-$arch.$suf "$TARGET"
fi
