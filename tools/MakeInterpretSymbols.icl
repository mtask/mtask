module MakeInterpretSymbols

import StdEnv
import GenType
import GenType.CCode
import Data.Either
import System.CommandLine
import System.FilePath

import mTask.Language => qualified :: MTask
import Data.UInt
import mTask.Interpret.Instructions
import mTask.Interpret.VoidPointer
import mTask.Interpret.Message
import mTask.Interpret.String255

derive gType BCInstrs, BCInstr, JumpLabel, BCTaskType, PinMode, I2CAddr, InterruptMode, Gesture
derive gType LEDMatrixInfo, DHTInfo, Pin, DHTtype, APin, DPin, MTaskValueState, BCPeripheral, MTaskEvalStatus
derive gType MTDeviceSpec, MTMessageFro, MTException, TaskValue, MTMessageTo, MTaskMeta, BCShareSpec, ButtonStatus, MTaskPrepData

forceMTaskPointer :: [Type] [[Type]] -> [[Type]]
forceMTaskPointer acc [[t]:ts]
	| isMember (typeName t) ["MTaskMeta", "MTMessageTo"]
		= forceMTaskPointer [t:acc] ts
forceMTaskPointer acc [t:ts] = [t:forceMTaskPointer acc ts]
forceMTaskPointer acc [] = [acc]

writeToFile :: [String] String *World -> *World
writeToFile data fp w
	# (ok, f, w) = fopen fp FWriteText w
	| not ok = abort ("Couldn't open " +++ fp +++ " for writing\n")
	# f = foldl (flip fwrites) f data
	# (ok, w) = fclose f w
	| not ok = abort ("Couldn't close " +++ fp +++ "\n")
	= w

Start world
	# ([argv0:args], world) = getCommandLine world
	| not (args =: [_])
		= abort "Please supply the target directory as the only argument\n"
	# td = hd args
	= case generateCCode opts types of
		Left e = abort ("Error generating c code: " +++ e +++ "\n")
		Right (h, c) = writeToFile h (td </> "types.h") (
			writeToFile c (td </> "types.c") world)
where
	opts = { zero
		& basename = "types"
		, customDeps = forceMTaskPointer []
		, overrides =
			[ uint8gType, uint16gType, uint32gType
			, int8gType, int16gType, int32gType
			, string255gType, bcinstrsgType, voidPointergType
			]
		}

	types :: Box GType (BCInstr, BCPeripheral, MTDeviceSpec, MTMessageFro, MTMessageTo, (PinMode, InterruptMode, ButtonStatus, Gesture))
	types = gType{|*|}
