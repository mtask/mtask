module MakeISR

import StdEnv
import Text
import Data.List
import Data.Maybe
import Data.Tree
import Data.Functor
import Data.Tuple
import Data.Encoding.RunLength
import qualified Data.Map as M
from Data.Map import :: Map, instance Functor (Map k)

//AVR uno
//pins_supported = [2, 3]
//modus_supported = ["CHANGE", "RISING", "FALLING", "LOW"]
//Feather m0
//pins_supported = [0, 2, 4, 5, 12, 13, 14, 15]
//ESP32
pins_supported = [0,1,2,3,5,9,10,13,25,26,27]
modus_supported = ["CHANGE", "RISING", "FALLING", "LOW", "HIGH"]

pins = [0..(last pins_supported)]
modus = ["CHANGE", "RISING", "FALLING", "LOW", "HIGH"]

// Helpers
generateFunction :: String [String] -> String
generateFunction signature body = signature +++ " { \n" +++ join "\n" ["\t" +++ line \\ line <- body] +++ "\n}"

modeToEnum :: String -> String
modeToEnum m = upperCaseFirst (toLowerCase m) +++ "_c"

// ISR
generateISRs :: String
generateISRs = join "\n" [generateISR (toString p) m \\ p <- pins, m <- modus]

generateISR :: String String -> String
generateISR p m = generateFunction signature body
	where signature = "IRAM_ATTR void isr_" +++ p +++ "_" +++ m +++ "()"
	      body = ["real_isr(" +++ p +++ ", " +++ modeToEnum m +++ ");"]

// Get ISR
generateISRMap :: String
generateISRMap = start +++ pins_array +++ end
	where start = "static void (*isr_map[" +++ toString ((last pins_supported + 1) * 5) +++ "])() = {\n"
	      pins_array = join ",\n" ["\t" +++ modus_array p \\ p <- pins]
	      modus_array p = "// Pin " +++ toString p +++ "\n\t" +++ join ", " [if (supported p m) ("isr_" +++ toString p +++ "_" +++ m) "NULL" \\ m <- modus]
	      supported p m = (isMember p pins_supported) && (isMember m modus_supported)
	      end = "\n};\n"

Start 
  = "\n\n//ISRS\n\n" 
  +++ generateISRs 
  +++ "\n\n//ISR map\n\n" 
  +++ generateISRMap
