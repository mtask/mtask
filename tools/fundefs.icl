module fundefs

import StdEnv

fun a = "\t& fun " +++ a +++ " v\n"

Start = 
	   [fun "()"]
	++ [fun ("(v " +++ x +++ ")")\\x<-types]
	++ [fun ("(v " +++ x +++ ", v " +++ y +++ ")")\\x<-types, y<-types]
	++ [fun ("(v " +++ x +++ ", v " +++ y +++ ", v " +++ z +++ ")")\\x<-types, y<-types, z<-types]

types = ["DPin", "Bool", "Char", "Int", "Long", "Real"]
