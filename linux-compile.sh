#!/bin/bash
set -e

MAKEFLAGS=${MAKEFLAGS:--j}

if [ $# -eq 2 ]
then
	if [ $1 = "-f" ]
	then
		FORCE=true
		MAKEFLAGS="$MAKEFSLAGS -B"
	else
		echo "Usage: $0 [-f]"
		exit 1
	fi
fi

for pkg in client server examples
do
	(
		cd $pkg
		FORCE="$FORCE" MAKEFLAGS="$MAKEFLAGS" ./linux-compile.sh
	)
done
